﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Assignment3.Model;

namespace Assignment3.Utils {

    public class MethodJsonConverter : JsonConverter<Request.Method?> {

        public override Request.Method? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            Request.Method? method;

            if (reader.TokenType == JsonTokenType.Null)
                method = null;

            else if (Enum.TryParse(reader.GetString(), true, out Request.Method methodTmp))
                method = methodTmp;

            else
                method = Request.Method.Illegal;

            return method;
        }

        public override void Write(Utf8JsonWriter writer, Request.Method? value, JsonSerializerOptions options) {
            throw new NotImplementedException();
        }
    }
}
