﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Assignment3.Utils {

    public class UnixDateJsonConverter : JsonConverter<long?> {

        public override long? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            long? value;

            if (reader.TokenType == JsonTokenType.Null) // date is null
                value = null;

            else if (long.TryParse(reader.GetString(), out long result)) // Could parse a date
                value = result;

            else    // could not parse any date
                value = 0;

            return value;
        }

        public override void Write(Utf8JsonWriter writer, long? value, JsonSerializerOptions options) {
            throw new NotImplementedException();
        }
    }
}
