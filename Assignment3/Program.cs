﻿using System;
using Assignment3.Network;

namespace Assignment3 {

    public class Program {

        public static void Main() {
            var server = new Server();
            server.Start();
        }
    }
}
