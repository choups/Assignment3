﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Assignment3.Model;
using System.Text.Json;
using System.Text.Json.Serialization;
using Assignment3.Utils;
using System.IO;

namespace Assignment3.Network {

    public class Server {

        // Properties
        private static int nbConnections = 0;
        public TcpListener listener { get; }
        private RequestManager requestManager;


        // Constructor
        public Server() {
            try {
                // Set the port on 5000
                int port = 5000;

                // Set the address to localhost
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                // Attempt to create an instance of TcpListener
                listener = new TcpListener(localAddr, port);

                // Create the RequestManager
                requestManager = new RequestManager();

            } catch (Exception e) {
                listener = null;
                Console.WriteLine($"Error while creating server: {e.Message}");
            }
        }


        public void Start() {

            if (listener == null) {
                throw new Exception("TcpListener could not be created");

            } else {

                try {
                    // Start to listen
                    listener.Start();

                    Console.Write("Waiting for a connection...\n");

                    while (true) {
                        // Accept a pending connection request
                        var client = listener.AcceptTcpClient();
                        new Thread(() => ProcessClient(client, ++nbConnections)).Start();
                    }

                } catch (SocketException e) {
                    Console.Write($"Error while listening: {e.Message}");

                } finally {
                    listener.Stop();
                }
            }
        }

        private void ProcessClient(TcpClient client, int connectionNumber) {

            try {
                Console.WriteLine($"Connection #{connectionNumber} established with client");

                // Buffer for reading data
                byte[] buffer = new byte[2048];

                NetworkStream stream = null;
                int i;

                if (client.Connected)
                    stream = client.GetStream();

                // Loop to receive all the data sent by the client.
                while (client.Connected && stream.CanRead && (i = stream.Read(buffer, 0, buffer.Length)) != 0) {
                    // Translate data bytes to a UTF-8 string.
                    var data = Encoding.UTF8.GetString(buffer, 0, i);

                    // Parse the request object
                    var request = JsonSerializer.Deserialize<Request>(data, new JsonSerializerOptions {
                        WriteIndented = true,
                        Converters = {
                            new UnixDateJsonConverter(),
                            new MethodJsonConverter()
                        }
                    });

                    // Process the request
                    var response = requestManager.Process(request);

                    // Stringify the response
                    string responseJson = JsonSerializer.Serialize(response, new JsonSerializerOptions() {
                        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    });

                    // Get the response json bytes
                    byte[] msg = Encoding.UTF8.GetBytes(responseJson);

                    // Send back the response
                    if (stream.CanWrite)
                        stream.Write(msg, 0, msg.Length);
                }

                // Close stream & client connection
                stream.Close();
                client.Close();
                Console.WriteLine($"Connection #{connectionNumber} ended with client");

            } catch (IOException) {
                Console.WriteLine($"Connection #{connectionNumber} ended by client");

            } catch (Exception e) {
                Console.WriteLine($"Error while trying to read client: {e.Message}");

            } finally {
                client.Close(); // Close client on server side even after an Exception
            }
        }
    }
}
