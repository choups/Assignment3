﻿using System;
using Assignment3.Controller;
using Assignment3.Model;
using System.Collections.Generic;

namespace Assignment3.Network {

    /**
     * This class manages the requests it receives from the server
     */
    public class RequestManager {

        /**
         * This enum is here in case we wanted to create other services
         */

        private enum Resource {
            Categories
        }


        // Public methods

        public Response Process(Request request) {
            Response response;
            var reasons = FirstCheck(request);

            // If there is some problems, we end it here
            if (reasons.Count > 0)
                response = new Response(Response.StatusCode.BadRequest, reasons: reasons);

            // We have done all the checkup to ensure that we can process the echo request
            else if (request.method == Request.Method.Echo)
                response = new Response(Response.StatusCode.Ok, body: request.body);

            // Else we can continue to process
            else {
                string[] pathSubstrings = request.path.Split('/'); // path format (api/service/resource)

                if (IdHasProblem(request.method, pathSubstrings))
                    response = new Response(Response.StatusCode.BadRequest);

                else {
                    AbstractController controller;

                    if (Enum.TryParse<Resource>(pathSubstrings[1], true, out _)) {

                        switch (Enum.Parse<Resource>(pathSubstrings[1], true)) {

                            case Resource.Categories:
                                controller = new CategoryController();
                                break;

                            default:
                                // This has been already handled but we need to declare a default case
                                controller = null;
                                break;
                        }

                        switch (request.method) {

                            case Request.Method.Create:
                                response = controller.Create(request.body);
                                break;

                            case Request.Method.Read:
                                response = (pathSubstrings.Length == 3) ? controller.Read(int.Parse(pathSubstrings[2])) : controller.ReadAll();
                                break;

                            case Request.Method.Update:
                                // id will always have a value here, no need to check anything
                                response = controller.Update(int.Parse(pathSubstrings[2]), request.body);
                                break;

                            case Request.Method.Delete:
                                // same thing as above
                                response = controller.Delete(int.Parse(pathSubstrings[2]));
                                break;

                            default:
                                // This has been already handled but we need to declare a default case
                                response = new Response(Response.StatusCode.BadRequest);
                                break;
                        }

                    } else {
                        response = new Response(Response.StatusCode.BadRequest);
                    }
                }
            }

            return response;
        }


        /**
         * Basic request data check
         */
        private List<string> FirstCheck(Request request) {
            List<string> reasons = new List<string>();

            // Checks if method is missing
            if (request.method == null)
                reasons.Add("missing method");

            // Checks if method is valid
            else if (request.method == Request.Method.Illegal)
                reasons.Add("illegal method");

            // Checks path absence validity -- the only valid case is if the method is "echo"
            if (request.path == null && request.method != Request.Method.Echo)
                reasons.Add("missing resource");

            // Checks if date is missing
            if (request.date == null)
                reasons.Add("missing date");

            // Checks if date is valid
            else if (request.date == 0)
                reasons.Add("illegal date");

            // Checks if body absence is valid -- the only valid cases are if the method is "read" or "echo"
            bool shouldHaveBody = (request.method == Request.Method.Create || request.method == Request.Method.Update || request.method == Request.Method.Echo);
            if (request.body == null && shouldHaveBody)
                reasons.Add("missing body");

            // Checks resource with path format (api/service/resource)
            if (request.path != null) {
                string[] pathSubstrings = request.path.Split('/');

                if (pathSubstrings.Length < 2 || !pathSubstrings[0].Equals("api"))
                    reasons.Add("illegal resource");
            }

            return reasons;
        }


        /*
         * Checks If the id is not here when it should OR If the id is here when it shouldn't be OR If the id can't be parsed when it has to be
         */
        private bool IdHasProblem(Request.Method? method, string[] pathSubstrings) {
            // Controls if the "id" is here in case the method is update or delete
            bool isUpdate = method == Request.Method.Update;
            bool isDelete = method == Request.Method.Delete;
            bool isRead = method == Request.Method.Read && pathSubstrings.Length == 3;
            bool shouldHaveId = isUpdate || isDelete || isRead;

            bool shouldHaveIdButDoesNot = shouldHaveId && pathSubstrings.Length < 3;
            bool shouldNotHaveIdButDoes = !shouldHaveId && pathSubstrings.Length > 2;
            bool doesHaveId = shouldHaveId && pathSubstrings.Length == 3;

            bool doesHaveIdButCannotParse = doesHaveId && !int.TryParse(pathSubstrings[2], out _);

            return shouldHaveIdButDoesNot || shouldNotHaveIdButDoes || doesHaveIdButCannotParse;
        }
    }
}
