﻿using System;
using Assignment3.Model;
using System.Text.Json;
using System.Collections.Generic;

namespace Assignment3.Controller {

    public class CategoryController: AbstractController {

        override public Response ReadAll() {
            var categoryList = Database.shared.ReadAll();
            var response = new Response(Response.StatusCode.Ok, body: JsonSerializer.Serialize(categoryList));

            return response;
        }


        override public Response Read(int id) {
            var category = Database.shared.Read(id);
            Response response;

            if (category == null)
                response = new Response(Response.StatusCode.NotFound);
            else
                response = new Response(Response.StatusCode.Ok, body: JsonSerializer.Serialize(category));

            return response;
        }


        override public Response Create(string payload) {
            Response response;

            try {
                var category = JsonSerializer.Deserialize<Category>(payload);
                int id = Database.shared.Create(category.name);
                response = new Response(Response.StatusCode.Created, body: JsonSerializer.Serialize(new Category(category.name, id)));

            } catch {
                var reasons = new List<string>() {
                    "illegal body"
                };

                response = new Response(Response.StatusCode.BadRequest, reasons: reasons);
            }

            return response;
        }


        override public Response Update(int id, string payload) {
            Response response;

            try {
                Response.StatusCode statusCode;

                var category = JsonSerializer.Deserialize<Category>(payload);
                var found = Database.shared.Update(id, category);

                if (!found)
                    statusCode = Response.StatusCode.NotFound;

                else if (id != category.cid)
                    statusCode = Response.StatusCode.BadRequest;

                else {
                    statusCode = Response.StatusCode.Updated;
                }

                response = new Response(statusCode);

            } catch {
                var reasons = new List<string>() {
                    "illegal body"
                };

                response = new Response(Response.StatusCode.BadRequest, reasons: reasons);
            }

            return response;
        }


        override public Response Delete(int id) {
            var found = Database.shared.Delete(id);
            var status = found ? Response.StatusCode.Ok : Response.StatusCode.NotFound;

            return new Response(status);
        }
    }
}
