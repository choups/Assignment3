﻿using System;
using Assignment3.Model;

namespace Assignment3.Controller {

    public abstract class AbstractController {
        public abstract Response ReadAll();
        public abstract Response Read(int id);
        public abstract Response Create(string payload);
        public abstract Response Update(int id, string payload);
        public abstract Response Delete(int id);
    }
}
