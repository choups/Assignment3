﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Assignment3.Model {

    public class Category {
        


        [JsonInclude]
        public int cid { get; }

        [JsonInclude]
        public string name { get; }


        [JsonConstructor]
        public Category(string name, int cid = 0) {
            this.cid = cid;
            this.name = name;
        }
    }
}
