﻿using System;
using System.Collections.Generic;

namespace Assignment3.Model {

    public sealed class Database {
        private static int nextCid = 1;
        private static Database instance = null;

        private Dictionary<int, string> categories;
        private static readonly object categoriesLock = new object();


        public static Database shared {
            get {
                lock (categoriesLock) {
                    if (instance == null)
                        instance = new Database();

                    return instance;
                }
            }
        }


        private Database() {
            categories = new Dictionary<int, string>();

            Category c1 = new Category("Beverages");
            Category c2 = new Category("Condiments");
            Category c3 = new Category("Confections");

            categories.Add(nextCid++, c1.name);
            categories.Add(nextCid++, c2.name);
            categories.Add(nextCid++, c3.name);
        }


        public List<Category> ReadAll() {
            List<Category> categories = new List<Category>();

            foreach (var couple in this.categories) {
                categories.Add(new Category(couple.Value, couple.Key));
            }

            return categories;
        }

#nullable enable
        public Category? Read(int id) {
            Category? category = null;
            var name = categories.GetValueOrDefault(id);

            if (name != null)
                category = new Category(name, id);

            return category;
        }
#nullable disable

        public int Create(string name) {
            int id = nextCid++;

            categories.Add(id, name);

            return id;
        }

        public bool Update(int id, Category category) {
            bool found = categories.ContainsKey(id);

            if (found)
                categories[id] = category.name;

            return found;
        }

        public bool Delete(int id) {
            return categories.Remove(id);
        }
    }
}
