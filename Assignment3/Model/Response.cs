﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Assignment3.Model {

    public class Response {

        public enum StatusCode : int {
            Ok = 1,
            Created = 2,
            Updated = 3,
            BadRequest = 4,
            NotFound = 5,
            Error = 6
        }


        [JsonIgnore]
        private List<string> reasons { get; }

        [JsonIgnore]
        private StatusCode statusCode { get; }

        [JsonInclude]
        public string body { get; }

        [JsonInclude]
        public string status {
            get {
                string val = statusCode.ToString("d");

                if (reasons != null && reasons.Count > 0) {

                    foreach (var reason in reasons)
                        val += " " + reason;

                } else {

                    switch (statusCode) {
                        case StatusCode.BadRequest:
                            val += " Bad Request";
                            break;

                        case StatusCode.NotFound:
                            val += " Not Found";
                            break;

                        default:
                            val += " " + statusCode.ToString("g");
                            break;
                    }
                }   

                return val;
            }
        }


        public Response(StatusCode statusCode, string body = null, List<string> reasons = null) {
            this.statusCode = statusCode;
            this.body = body;

            if (reasons != null)
                this.reasons = reasons;
        }
    }
}