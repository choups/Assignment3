﻿using System;
using System.Text.Json.Serialization;
using Assignment3.Utils;

namespace Assignment3.Model {

    public class Request {

        [JsonConverter(typeof(MethodJsonConverter))]
        public enum Method {
            Create,
            Read,
            Update,
            Delete,
            Echo,
            Illegal
        }


        [JsonInclude]
        public string path { get; }

        [JsonInclude]
        public string body { get; }

        [JsonInclude]
        public Method? method { get; }

        [JsonInclude]
        [JsonConverter(typeof(UnixDateJsonConverter))]
        public long? date { get; }


        [JsonConstructor]
        public Request(Method? method, long? date, string body, string path) {
            this.method = method;
            this.date = date;

            if (path != null && path.StartsWith('/'))
                path = path.Remove(0, 1);

            if (path != null && path.EndsWith('/'))
                path = path.Remove(path.Length - 1, 1);

            this.path = path;
            this.body = body;
        }


        // FIXME: Temp method
        public override string ToString() {
            return $"{method}, {path}, {date}, {body}";
        }
    }
}